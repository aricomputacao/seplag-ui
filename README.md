
Projeto utilizado na seleção para à vaga de desenvolvedor da Cotec/Seplag

# Visão geral

O projeto é uma aplicação front-end  com objetivo de criar uma interface em [Angular](https://angular.io/), [PrimeNg](https://www.primefaces.org/primeng/setup) para usuários consultar as atualizações da "Microsoft Security Updates"

## Tecnologias

- [Angular](https://angular.io/) é um framework JavaScript de código aberto mantido pela Google para a construção de SPA (sigla para Single Page Applications ou Aplicações de Página Única).

- [PrimeNg](https://www.primefaces.org/primeng/setup) é uma bliblioteca rica em componentes para utilizada para desenvolver aplicações Angular.

# Setup da aplicação (local)

## Pré-requisito

Antes de rodar a aplicação é preciso garantir que as seguintes dependências estejam corretamente instaladas:

```
Angular 14
Primene 14
Node 14 
NPM 8
```

## Funcionalidades

- Consultar os registros de atualizações. 
- Remover registros de atualização.


## Instalação da aplicação

Primeiramente, faça o clone do repositório:

```
https://gitlab.com/aricomputacao/seplag-ui.git
```

Feito isso, acesse o projeto:

```
cd seplag-ui
```

É preciso instalar as dependencias do projeto:

```
npm install
```

É preciso gerar a pasta /dist do projeto:

```
ng build
```

Finalizado esse passo, vamos iniciar a aplicação:

```
node server.js
```

Pronto. A aplicação está disponível em http://localhost:4200



# Paginas

O projeto disponibiliza duas páginas com consultas ao serviço de back-end.

Segue abaixo as páginas disponíveis no projeto:

#### Home

- Contem uma visão de quantas atualizações foram registradas por ano.


#### Atualizações

- Contem uma consulta com filtros das atualizações registradas.


