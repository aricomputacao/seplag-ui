
import { FormsModule } from '@angular/forms';
import { AppNotfoundComponent } from './pages/nao-entrado/app.notfound.component';
import { CoreModule } from './core/core.module';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import {PasswordModule} from 'primeng/password';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppMainComponent } from './app.main.component';

import { AppErrorComponent } from './pages/erro/app.error.component';
import { AppAccessdeniedComponent } from './pages/acesso-negado/app.accessdenied.component';

import { SharedModule } from './shared/shared.module';
import { SegurancaModule } from './seguranca/seguranca.module';
import { HomePage } from './pages/home/home.page';



export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [

    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    FormsModule,
    SegurancaModule,
    PasswordModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient],
      },
      defaultLanguage: 'pt-BR',}),


  ],
  declarations: [
    AppComponent,
    AppMainComponent,
    AppNotfoundComponent,
    AppErrorComponent,
    AppAccessdeniedComponent,
    HomePage,

  ],

  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
