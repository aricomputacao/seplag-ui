import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  layoutMode = 'static';

  layoutColor = 'light';

  darkMenu = false;

  profileMode = 'inline';

  ripple = false;

  inputStyle = 'filled';

  dialogModal = false;

  constructor(

    private primengConfig: PrimeNGConfig,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.primengConfig.ripple = true;
    this.translateService.get('primeng').subscribe(res => this.primengConfig.setTranslation(res));
  }


}
