import { ReactiveFormsModule } from '@angular/forms';
import { InputNumberModule } from 'primeng/inputnumber';
import { CalendarModule } from 'primeng/calendar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AtualizacaoConsultaComponent } from './components/atualizacao-consulta/atualizacao-consulta.component';
import { AtualizacaoPage } from './containers/atualizacao/atualizacao.page';

import {DropdownModule} from 'primeng/dropdown';


@NgModule({
  declarations: [
    AtualizacaoConsultaComponent,
    AtualizacaoPage,

  ],
  imports: [
    RouterModule.forChild([
      {path: '',component: AtualizacaoPage},
    ]),
    CommonModule,
    SharedModule,
    SplitButtonModule,
    CalendarModule,
    InputNumberModule,
    ReactiveFormsModule,
    DropdownModule
  ]
})
export class AtualizacaoModule { }
