import { LabelService } from 'src/app/shared/services/label.service';
import { MenuItem, LazyLoadEvent } from 'primeng/api';
import { Atualizacao } from '../../../../shared/models/atualizacao.moldel';
import { Component, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
  selector: 'app-atualizacao-consulta',
  templateUrl: './atualizacao-consulta.component.html',
  styleUrls: ['./atualizacao-consulta.component.scss']
})
export class AtualizacaoConsultaComponent implements OnInit {

  @Input() loading: boolean = true;
  @Input() lista: Atualizacao[];
  @Input() pagina;
  @Input() itensPorPagina;
  @Input() totalDeRegistros;
  @Input() items: MenuItem[];


  @Output() consultar = new EventEmitter();
  @Output() aoMudarPagina = new EventEmitter();
  @Output() excluir = new EventEmitter();
  @Output() onDropdownClick = new EventEmitter();


  @ViewChild('tabela') grid;


  constructor(public label: LabelService) { }

  ngOnInit(): void {

  }

  consultarAtualizacao() {
    this.consultar.emit(this.pagina);
  }


  mudarPagina(event: LazyLoadEvent) {
    this.aoMudarPagina.emit(event);

  }

  excluirRegistro(atualizacao: Atualizacao) {
    this.excluir.emit(atualizacao);
  }



  aoClicarNoSplit(atualizacao: Atualizacao) {
    this.onDropdownClick.emit(atualizacao);
  }

}
