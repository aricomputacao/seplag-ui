import { DadosInformacaoId } from './../../../../shared/models/dados-informacao-id.imodel';
import { AtualizacaoConsultaComponent } from './../../components/atualizacao-consulta/atualizacao-consulta.component';
import { AtualizacaoFiltro } from './../../../../shared/models/filtros/atualizacao-filtro.model';
import { Atualizacao } from '../../../../shared/models/atualizacao.moldel';
import { ErrorHandlerService } from './../../../../core/error-handler.service';
import { AppComponent } from 'src/app/app.component';
import { Title } from '@angular/platform-browser';
import { LabelService } from 'src/app/shared/services/label.service';
import { MenuItem, MessageService, ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AtualizacaoService } from 'src/app/shared/services/atualizacao.service';
import { MensagemService } from 'src/app/shared/services/mensagem.service';


@Component({
  selector: 'app-atualizacao',
  templateUrl: './atualizacao.page.html',
  styleUrls: ['./atualizacao.page.scss']
})
export class AtualizacaoPage implements OnInit {


  loading: boolean = true;

  atualizacao = new Atualizacao();

  minDate: Date;
  maxDate = new Date();

  listaDeId: DadosInformacaoId[];
  listaDeAtualizacoes: [];

  items: MenuItem[];


  atualzacaoFiltro = new AtualizacaoFiltro();
  pagina = 0;
  itensPorPagina = 10;
  totalDeRegistros = 0;

  btnSalvar: boolean;
  btnNovo: boolean;
  btnExcluir: boolean;





  @ViewChild(AtualizacaoConsultaComponent)
  atualizacaoConsulta: AtualizacaoConsultaComponent;


  constructor(
    public label: LabelService,
    private errorHandler: ErrorHandlerService,
    private messageService: MessageService,
    private mensagem: MensagemService,
    private confirmationService: ConfirmationService,
    private title: Title,
    private atualizacaoService: AtualizacaoService,
    public app: AppComponent,
  ) { }




  ngOnInit(): void {
    this.loading = false;
    this.title.setTitle(this.label.siglaSistema + ' | ' + this.label.gerenciar + ' ' + this.label.atualizacao)

    this.atualizacaoService.consultarIds()
      .subscribe(
        (dados) => {
          this.listaDeId = dados;
        }
      );

  }




  consultar(pagina = 0) {


    this.pagina = pagina;
    this.loading = true;
    this.atualizacaoService.consultar(this.atualzacaoFiltro, this.pagina, this.itensPorPagina)
      .subscribe(
        (resultado) => {
          this.listaDeAtualizacoes = resultado.atualizacoes;
          this.totalDeRegistros = resultado.total;

        },
        (error) => {
          this.loading = false;
          this.errorHandler.handle(error);
        },
        () => {
          this.montarItems();
          this.loading = false;
        }

      );


  }

  abrirItens(atualizacao: Atualizacao): void {
    this.atualizacao = atualizacao;
  }

  aoMudarPagina(event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.itensPorPagina = event.rows;
    this.consultar(pagina);
  }


  confirmarExclusao(atualizacao: Atualizacao) {
    this.confirmationService.confirm({
      message: this.mensagem.confirmacaoExclusao,
      accept: () => {
        this.excluir(atualizacao);
      }
    });
  }

  excluir(atualizacao: Atualizacao) {
    this.atualizacaoService.excluir(atualizacao.ID)
      .subscribe(
        () => {
          this.consultar(this.pagina);
          this.messageService.add({ severity: 'success', summary: 'Informação', detail: this.mensagem.registroSucesso })
        },
        (error) => {
          this.errorHandler.handle(error);
        }
      )
  }




  private montarItems() {

    this.items = [

      {
        label: this.label.excluir, icon: 'pi pi-trash', styleClass: 'red-btn-acao', tabindex: '1',
        command: () => {
          this.confirmarExclusao(this.atualizacao);
        }
      },

    ];


  }
}
