import { LabelService } from './../../shared/services/label.service';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accessdenied',
  templateUrl: './app.accessdenied.component.html',
})
export class AppAccessdeniedComponent  implements OnInit{

  constructor( private title: Title, private label: LabelService){

  }

  ngOnInit(): void {
    // this.translateService.get('primeng').subscribe(res => this.primengConfig.setTranslation(res));
    this.title.setTitle(this.label.siglaSistema + ' | ' + this.label.acesso + ' ' + this.label.negado)
  }


}
