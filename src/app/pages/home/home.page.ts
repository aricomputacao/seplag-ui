import { AtualizacaoService } from 'src/app/shared/services/atualizacao.service';
import { DadosQuantidadeExercicio } from './../../shared/models/dados-quantidade-exercicio.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})
export class HomePage implements OnInit {


  listaQuantidade: DadosQuantidadeExercicio[];

  constructor(private atualizacaoService: AtualizacaoService) { }

  ngOnInit(): void {
    this.atualizacaoService.consultarQuantidadeAnos().subscribe(
      (dados) => {

        this.listaQuantidade = dados;
      }
      );
      console.log(this.listaQuantidade);

  }

  processarClasse(index: number){
    console.log(index)
    if(index % 2  === 0){
      return "widget-overview overview-box-1"
    }else{
      return "widget-overview overview-box-3"
    }
  }

}
