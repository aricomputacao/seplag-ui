import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';


import { VazioComponent } from './pages/vazio/vazio.component';
import { AppErrorComponent } from './pages/erro/app.error.component';
import { AppAccessdeniedComponent } from './pages/acesso-negado/app.accessdenied.component';
import { AppMainComponent } from './app.main.component';
import { HomePage } from './pages/home/home.page';




const routes: Routes = [
  {
    path: '', component: AppMainComponent,
    children: [
      {
        path: 'atualizacao',
        loadChildren: () => import('./pages/atualizacao/atualizacao.module').then(m => m.AtualizacaoModule),
      },


      {
        path: '',
        component: HomePage,
      },

    ]
  },
  { path: 'error', component: AppErrorComponent },
  { path: 'accessdenied', component: AppAccessdeniedComponent },
  { path: 'notfound', component: VazioComponent },
  { path: '**', redirectTo: '/notfound' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
     )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
