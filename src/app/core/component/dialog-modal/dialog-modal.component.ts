import { LabelService } from './../../../shared/services/label.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialog-modal',
  templateUrl: './dialog-modal.component.html',
  styleUrls: ['./dialog-modal.component.scss']
})
export class DialogModalComponent implements OnInit {

  @Input() displayModal: boolean;

  constructor(public label: LabelService) { }

  ngOnInit(): void {
  }




}
