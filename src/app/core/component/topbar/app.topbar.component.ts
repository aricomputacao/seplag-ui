import { AppMainComponent } from './../../../app.main.component';
import { AppComponent } from './../../../app.component';
import { Component } from '@angular/core';


@Component({
  selector: 'app-topbar',
  template: `
        <div class="topbar clearfix">
            <div class="topbar-left">
                <a routerLink="/">
                    <img src="assets/layout/images/logo.png" class="topbar-logo" routerLink="/" />
                </a>
            </div>

            <div class="topbar-right">
                <a id="menu-button" href="#" (click)="appMain.onMenuButtonClick($event)"
                   [ngClass]="{'menu-button-rotate': appMain.rotateMenuButton}">
                    <i class="pi pi-angle-left"></i>
               </a>

                <a id="topbar-menu-button" href="#" (click)="appMain.onTopbarMenuButtonClick($event)">
                    <i class="pi pi-bars"></i>
                </a>

                <ul class="topbar-items fadeInDown" [ngClass]="{'topbar-items-visible': appMain.topbarMenuActive}">
                    <li #profile class="profile-item" *ngIf="app.profileMode==='top'|| appMain.isHorizontal()"
                        [ngClass]="{'active-top-menu':appMain.activeTopbarItem === profile}">

                        <a href="#" (click)="appMain.onTopbarItemClick($event,profile)">

                        <img class="profile-image"   [src]="auth.jwtPayload?.foto"  class="border-circle w-3rem h-3rem m-2  " />

                        <img class="profile-image" [src]="auth.jwtPayload?.foto"  class="border-circle w-3rem h-3rem m-2  " />
                            <div>
                                <span class="topbar-item-name">{{auth.jwtPayload?.nome}}</span>
                                <span class="topbar-item-role">{{auth.jwtPayload?.perfil}}</span>
                            </div>
                        </a>

                        <ul class="layout-menu" [ngClass]="{'fadeInDown':!appMain.isMobile()}">
                            <li role="menuitem">
                                <a href="#" (click)="appMain.onTopbarSubItemClick($event)">
                                    <i class="pi pi-fw pi-user"></i>
                                    <span>Profile</span>
                                </a>
                            </li>
                            <li role="menuitem">
                                <a href="#" (click)="appMain.onTopbarSubItemClick($event)">
                                    <i class="pi pi-fw pi-lock"></i>
                                    <span>Privacy</span>
                                </a>
                            </li>
                            <li role="menuitem">
                                <a href="#" (click)="appMain.onTopbarSubItemClick($event)">
                                    <i class="pi pi-fw pi-cog"></i>
                                    <span>Settings</span>
                                </a>
                            </li>
                            <li role="menuitem">
                                <a href="#" (click)="appMain.onTopbarSubItemClick($event)">
                                    <i class="pi pi-fw pi-sign-out"></i>
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li #notifications [ngClass]="{'active-top-menu':appMain.activeTopbarItem === notifications}">
                        <a href="#" (click)="appMain.onTopbarItemClick($event,notifications)">
                            <i class="topbar-icon pi pi-fw pi-bell"></i>
                            <span class="topbar-badge animated rubberBand">4</span>
                            <span class="topbar-item-name">Notifications</span>
                        </a>
                        <ul class="layout-menu" [ngClass]="{'fadeInDown':!appMain.isMobile()}">
                            <li role="menuitem">
                                <a href="#" (click)="appMain.onTopbarSubItemClick($event)">
                                    <i class="pi pi-fw pi-sliders-h"></i>
                                    <span>Pending tasks</span>
                                </a>
                            </li>
                            <li role="menuitem">
                                <a href="#" (click)="appMain.onTopbarSubItemClick($event)">
                                    <i class="pi pi-fw pi-calendar"></i>
                                    <span>Meeting today at 3pm</span>
                                </a>
                            </li>
                            <li role="menuitem">
                                <a href="#" (click)="appMain.onTopbarSubItemClick($event)">
                                    <i class="pi pi-fw pi-download"></i>
                                    <span>Download documents</span>
                                </a>
                            </li>
                            <li role="menuitem">
                                <a href="#" (click)="appMain.onTopbarSubItemClick($event)">
                                    <i class="pi pi-fw pi-bookmark"></i>
                                    <span>Book flight</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    `
})
export class AppTopBarComponent {

  constructor(
    public app: AppComponent,
    public appMain: AppMainComponent,
    ) { }

}
