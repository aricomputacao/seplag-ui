import { LabelService } from './../../../shared/services/label.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-menu',
  template: `
    <ul class="layout-menu layout-main-menu clearfix">
        <li app-menuitem *ngFor="let item of model; let i = index;" [item]="item" [index]="i" [root]="true"></li>
    </ul>
    `
})
export class AppMenuComponent implements OnInit {

  model: any[];


  constructor(
    private label: LabelService,
  ) {

  }

  ngOnInit() {

    this.model = [

      {
        label: 'Menu',
        items: [
          { label: 'Home', icon: 'pi pi-fw pi-home', routerLink: ['/'],  },
          { label: 'Atualizações', icon: 'pi pi-fw pi-refresh', routerLink: ['/atualizacao'],  },


        ]
      },



    ];
  }
}
