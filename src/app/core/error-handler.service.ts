import { MessageService } from 'primeng/api';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(
    private messageService: MessageService,
  ) { }

  handle(errorResponse: any) {

    let msg: string;
    let status = errorResponse['status'];

    if (typeof errorResponse === 'string') {
      msg = errorResponse;

    } else if (errorResponse instanceof HttpErrorResponse && status >= 400 && status <= 499) {
      try {
        if (errorResponse.error.error === 'invalid_grant') {
          msg = errorResponse.error.error_description;
        } else {
          if (status === 404) {
            msg = 'Erro ao processar serviço remoto. Tente novamente!'
          }
          msg = errorResponse['error'][0].mensagemUsuario;

        }
        if (status === 403) {
          msg = 'Você não tem permissão para executar essa ação!'
        }

      } catch (e) { }
    } else {

      msg = 'Erro ao processar serviço remoto. Tente novamente.'
    }

    this.messageService.add({ severity: 'error', summary: 'Informação', detail: msg })
  }
}
