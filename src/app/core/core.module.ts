import { ErrorHandlerService } from './error-handler.service';
import { ToastModule } from 'primeng/toast';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RadioButtonModule } from 'primeng/radiobutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { DialogModule } from 'primeng/dialog';



import { AppTopBarComponent } from './component/topbar/app.topbar.component';
import { AppFooterComponent } from './component/footer/app.footer.component';
import { MenuService } from './component/menu/app.menu.service';
import { AppMenuitemComponent } from './component/menu/app.menuitem.component';
import { AppMenuComponent } from './component/menu/app.menu.component';
import { DialogModalComponent } from './component/dialog-modal/dialog-modal.component';
import { AccordionModule } from 'primeng/accordion';
import { MessageService } from 'primeng/api';
import { Title } from '@angular/platform-browser';




@NgModule({
  declarations: [
    AppMenuComponent,
    AppMenuitemComponent,
    AppFooterComponent,
    AppTopBarComponent,
    DialogModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RadioButtonModule,
    InputSwitchModule,
    AccordionModule,
    RouterModule,
    DialogModule,
    ToastModule,


  ],
  exports: [AppMenuComponent, AppMenuitemComponent, AppFooterComponent, AppTopBarComponent,
      DialogModalComponent],
  providers: [
    MenuService,
    MessageService,
    ErrorHandlerService,
    Title,

  ]
})
export class CoreModule { }
