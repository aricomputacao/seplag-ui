import { environment } from './../../environments/environment';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordModule } from 'primeng/password';




@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    PasswordModule,
  ],
  declarations: [],
  providers: [
    // ...

  ]

})
export class SegurancaModule { }
