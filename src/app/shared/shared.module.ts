import { TranslateModule } from '@ngx-translate/core';
import { TableModule } from 'primeng/table';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { InputMaskModule } from 'primeng/inputmask';
import { FieldsetModule } from 'primeng/fieldset';
import { ToolbarModule } from 'primeng/toolbar';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import { ConfirmationService, MessageService } from 'primeng/api';

import { CalendarComponent } from './components/calendar/calendar.component';
import { MessageComponent } from './components/message/message.component';
import { DialogConfirmacaoComponent } from './components/dialog-confirmacao/dialog-confirmacao.component';



@NgModule({
  declarations: [CalendarComponent,
    MessageComponent,  DialogConfirmacaoComponent],
  imports: [
    CommonModule,
    FormsModule,
    InputTextModule,
    InputMaskModule,
    FieldsetModule,
    ToolbarModule,
    ButtonModule,
    TableModule,
    MessagesModule,
    MessageModule,
    CalendarModule,
    DropdownModule,
    RouterModule,
    TranslateModule,
    ToastModule,
    ConfirmDialogModule,



  ],
  exports: [
    TableModule,
    TranslateModule,
    FormsModule,
    ButtonModule,
    InputMaskModule,
    ToastModule,
    ConfirmDialogModule,
    InputTextModule,
    FieldsetModule,
    CalendarComponent,
    MessageComponent,
    DialogConfirmacaoComponent

  ],
  providers: [ConfirmationService]
})
export class SharedModule { }
