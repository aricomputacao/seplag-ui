import { Observable } from 'rxjs';
import { AtualizacaoFiltro } from './../models/filtros/atualizacao-filtro.model';
import { environment } from './../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AtualizacaoService {

  url: string;

  constructor(
    private http: HttpClient) {
    this.url = `${environment.apiUrl}/api/v1/atualizacoes`
  }

  consultar(atualizacaoFiltro: AtualizacaoFiltro, pagina: number, itensPorPagina: number): Observable<any> {


    let params = new HttpParams();

    params = params.set('pagina', pagina.toString());
    params = params.set('tamanho', itensPorPagina.toString());

    //CHECANDO SE ALGUM PARÂMETRO É PASSADO NO FILTRO PARA ALTERAR URL DA REQUISIÇÃO
    if (atualizacaoFiltro?.ID) {
      params = params.set('ID', atualizacaoFiltro.ID);
    }
    if (atualizacaoFiltro?.Alias) {
      params = params.set('Alias', atualizacaoFiltro.Alias);
    }
    if (atualizacaoFiltro?.DocumentTitle) {
      params = params.set('DocumentTitle', atualizacaoFiltro.DocumentTitle);
    }
    if (atualizacaoFiltro?.InitialReleaseDate) {
      params = params.set('InitialReleaseDate', atualizacaoFiltro.InitialReleaseDate);
    }
    if (atualizacaoFiltro?.CurrentReleaseDate) {
      params = params.set('CurrentReleaseDate', atualizacaoFiltro.CurrentReleaseDate);
    }
    if (atualizacaoFiltro?.CvrfUrl) {
      params = params.set('CvrfUrl', atualizacaoFiltro.CvrfUrl);
    }


    return this.http.get(this.url, { params })
      .pipe(
        map(
          response => {

            const resultado = {
              atualizacoes: response['content'],
              total: response['totalElements']
            }
            return resultado;
          })
      );

  }


  consultarIds(): Observable<any> {

    return this.http.get(`${this.url}/consultar-ids`)
      .pipe(
        map(
          response => {
            return response;
          })
      );

  }

  consultarQuantidadeAnos(): Observable<any> {

    return this.http.get(`${this.url}/consultar-quantidade-ano`)
      .pipe(
        map(
          response => {
            return response;
          })
      );

  }

  excluir(id: string): Observable<any> {
    let params = new HttpParams();

    params = params.set('id', id);


    return this.http.delete<void>(`${this.url}/${id}`, { params })
      .pipe();

  }

}
