import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MensagemService {

  constructor() { }

  get registroSucesso(){
    return 'Registro excluído com sucesso!';
  }

  get confirmacaoExclusao(){
    return 'Tem certeza que deseja excluir?';
  }

  get confirmacaoAtivacao(){
    return 'Tem certeza que deseja ativar o registro?';
  }

  get confirmacaoDesativacao(){
    return 'Tem certeza que deseja desativar o registro?';
  }
}
