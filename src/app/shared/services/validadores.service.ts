import { FormControl } from '@angular/forms';
import { Injectable } from '@angular/core';

import validator from 'cpf-cnpj-validator';
import { cnpj } from 'cpf-cnpj-validator';



@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {


  constructor() { }


  validarCnpj() {
    // valor = valor.replace(/[^0-9]/g,'');
    let  valor: string;
    return (input: FormControl) => {
      valor = input.value;
      return (cnpj.isValid(valor.replace(/[^0-9]/g,''))) ? null : { cnpjValidator:  true };
    };
  }
}
