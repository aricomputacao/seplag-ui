import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LabelService {

  constructor() { }


  get nomeSistema(): string{
    return 'SEPLAG';
  }

  get siglaSistema(): string{
    return 'SEPLAG';
  }


  get id(): string {
    return 'Id';
  }

  get gerenciar(): string {
    return 'Gerenciar';
  }

  get acao(): string {
    return 'Ação';
  }

  get atualizacao(): string {
    return 'Atualização';
  }

  get excluir(): string {
    return 'Excluir';
  }
  get acesso(): string {
    return 'Acesso';
  }
  get negado(): string {
    return 'Negado';
  }
  get consulta(): string {
    return 'Consulta';
  }


}
