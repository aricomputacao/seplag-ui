export class Atualizacao{
  ID?: string;
  Alias?: string;
  DocumentTitle?: string;
  Severity?: string;
  InitialReleaseDate?: string;
  CurrentReleaseDate?: string;
  CvrfUrl?: string;

}
