import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl, FormControl, UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-message',
  template: `
     <p-message *ngIf="temErro()" severity="error"
                  text="{{text}}"></p-message>
  `,
  styles: [
  ]
})
export class MessageComponent  {

  @Input() error: string;
  @Input() control?: UntypedFormControl | AbstractControl | FormControl | null;
  @Input() text: string;

  temErro(): boolean{
    return this.control ? this.control.hasError(this.error) && this.control.dirty : true;

    // return this.control.hasError(this.error) && this.control.dirty;
  }



}
