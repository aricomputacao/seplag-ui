import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  @Input() label: string;
  @Input() id: string;
  @Input() value: string;
  @Input() mensagem: string;
  @Input() disabled = false;
  @Input() require = false;
  @Input() showIcon = true;
  @Input() showButtonBar = true;
  @Input() touchUI = false;
  @Input() readonlyInput = false;
  @Input() minDate: Date;
  @Input() maxDate: Date;





  constructor() {
    this.label = 'input';
    this.id = 'calendar';
  }

  ngOnInit(): void {
  }

}
